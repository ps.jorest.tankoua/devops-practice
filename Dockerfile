FROM eclipse-temurin:8u382-b05-jdk-alpine

WORKDIR /opt/data

COPY target/*.jar .

RUN addgroup -S java && adduser -S java -G java

RUN chown -R java:java /opt/data

USER java

ARG APP
ARG DB

ENV APPFILE=$APP
ENV DATABASE=$DB

EXPOSE 8090

CMD java -server -Djava.net.preferIPv4Stack=true $DATABASE -jar $APPFILE

