#!/bin/bash

SHA=`git rev-parse --short=8 HEAD`
DATE=`git log -1 --pretty='format:%ad' --date=format:%y%m%d`

VERSION=$DATE-$SHA
IMAGE_NAME=assignment-$VERSION.jar
DOCKER_IMAGE=jobri237/assignment:$VERSION

echo VERSION=$VERSION > .env
echo IMAGE_NAME=$IMAGE_NAME >> .env
echo DOCKER_IMAGE=$DOCKER_IMAGE >> .env

read -p "Select a database to run the app: 1).H2  or  2).MYSQL: " num
echo ''

case $num in
	1) if [ -f "./target/$IMAGE_NAME" ]; then
		echo -e "*****Most recent artifcat exists*******\n"
	   else
	   	mvn clean package
		docker build -t $DOCKER_IMAGE --build-arg H2='-Dspring.profiles.active=h2' --build-arg APP=$IMAGE_NAME .
	   fi

	   docker run --rm -p 8090:8090 $DOCKER_IMAGE;;

	2) docker-compose up ;;
	*) echo 'Enter 1 or 2';;
esac

#git log -1 --pretty='format:%ad-%h' --date=format:%y%m%d

